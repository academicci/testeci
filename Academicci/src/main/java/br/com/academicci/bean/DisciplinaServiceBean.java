package br.com.academicci.bean;

import br.com.academicci.entity.Disciplina;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import org.omnifaces.util.Messages;

@ManagedBean
@ViewScoped
public class DisciplinaServiceBean implements Serializable {

    private Disciplina disciplina;
    private List<Disciplina> disciplinas;

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public void novo() {
        disciplina = new Disciplina();
    }

    @PostConstruct
    public void listar() {
        try {
            Client cliente = ClientBuilder.newClient();
            WebTarget caminho = cliente.target("http://localhost:8080/Academicci/rest/disciplina");
            String json = caminho.request().get(String.class);

            Gson gson = new Gson();
            Disciplina[] vetDisciplina = gson.fromJson(json, Disciplina[].class);

            disciplinas = Arrays.asList(vetDisciplina);
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os disciplinas");
            erro.printStackTrace();
        }
    }

    public void salvar() {
        try {
            Client cliente = ClientBuilder.newClient();
            WebTarget caminho = cliente.target("http://127.0.0.1:8080/Academicci/rest/disciplina");

            Gson gson = new Gson();

            String json = gson.toJson(disciplina);
            caminho.request().post(Entity.json(json));

            disciplina = new Disciplina();

            json = caminho.request().get(String.class);
            Disciplina[] vetDisciplina = gson.fromJson(json, Disciplina[].class);
            disciplinas = Arrays.asList(vetDisciplina);

            Messages.addGlobalInfo("Disciplina salva com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar a disciplina!");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            disciplina = (Disciplina) evento.getComponent().getAttributes().get("disciplinaSelecionada");

            Client cliente = ClientBuilder.newClient();

            WebTarget caminho = cliente.target("http://127.0.0.1:8080/Academicci/rest/disciplina");
            WebTarget caminhoExcluir = caminho.path("{id}").resolveTemplate("id", disciplina.getId());

            caminhoExcluir.request().delete();
            String json = caminho.request().get(String.class);

            Gson gson = new Gson();
            Disciplina[] vetDisciplina = gson.fromJson(json, Disciplina[].class);

            disciplinas = Arrays.asList(vetDisciplina);

            Messages.addGlobalInfo("Disciplina removida com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a disciplina!");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        disciplina = (Disciplina) evento.getComponent().getAttributes().get("disciplinaSelecionada");
    }
}
