package br.com.academicci.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.academicci.dao.PessoaDAO;

import br.com.academicci.entity.Pessoa;
import org.apache.shiro.crypto.hash.SimpleHash;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class PessoaBean implements Serializable {

    private Pessoa pessoa;
    private List<Pessoa> pessoas;

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    @PostConstruct
    public void listar() {
        try {
            PessoaDAO pessoaDAO = new PessoaDAO();
            pessoas = pessoaDAO.listar();
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar as pessoas");
            erro.printStackTrace();
        }
    }

    public void novo() {
        try {
            pessoa = new Pessoa();

        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar gerar uma nova pessoa");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        try {
            pessoa = (Pessoa) evento.getComponent().getAttributes().get("pessoaSelecionada");

            Messages.addGlobalInfo("Pessoa editada com sucesso ");
        } catch (RuntimeException e) {
            Messages.addGlobalError("Erro ao editar a 'Pessoa' ");
            e.printStackTrace();
        }

    }

    public void salvar() {

        try {
            PessoaDAO dao = new PessoaDAO();
            
            SimpleHash simpleHash = new SimpleHash("md5", pessoa.getSenhaSemCriptografia());
            pessoa.setSenha(simpleHash.toHex());
            
            dao.merge(pessoa);

            pessoas = dao.listar("nome");
            pessoa = new Pessoa();

            Messages.addGlobalInfo("Pessoa Salva com sucesso");
        } catch (RuntimeException e) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar pessoa");
        }

    }

    public void excluir(ActionEvent evento) {
        try {
            pessoa = (Pessoa) evento.getComponent().getAttributes().get("pessoaSelecionada");

            PessoaDAO pessoaDAO = new PessoaDAO();
            pessoaDAO.excluir(pessoa);

            pessoas = pessoaDAO.listar();

            Messages.addGlobalInfo("Pessoa removida com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Pessoa");
            erro.printStackTrace();
        }
    }

}
