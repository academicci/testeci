/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.bean;

import br.com.academicci.dao.DisciplinaDAO;
import br.com.academicci.dao.MonitoriaDAO;
import br.com.academicci.entity.Disciplina;
import br.com.academicci.entity.Monitoria;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author alexa
 */
@ManagedBean
@ViewScoped
public class MonitoriaBean {
    
    private List<Monitoria> monitorias;
    private Monitoria monitoria;
    private List<Disciplina> disciplinas;
    

    public Monitoria getMonitoria() {
        return monitoria;
    }

    public void setMonitoria(Monitoria monitoria) {
        this.monitoria = monitoria;
    }

    public List<Monitoria> getMonitorias() {
        return monitorias;
    }

    public void setMonitorias(List<Monitoria> monitorias) {
        this.monitorias = monitorias;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }
    
    
    public void novo() {
        try {
            monitoria = new Monitoria();
            
            DisciplinaDAO ddao = new DisciplinaDAO();
            disciplinas = ddao.listar("nome");
            
        } catch (RuntimeException e) {
            Messages.addGlobalError("Erro ao adicionar uma nova monitoria");
        }
        
    }

    public void salvar() {
        try {
            MonitoriaDAO monitoriaDAO = new MonitoriaDAO();
            monitoriaDAO.merge(monitoria);

            monitoria = new Monitoria();
			
			DisciplinaDAO ddao = new DisciplinaDAO();
			disciplinas = ddao.listar("nome");
			
			monitorias = monitoriaDAO.listar("nome");
			
			Messages.addGlobalInfo("Monitoria Salva com sucesso");
                        
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar a Monitoria!");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            monitoria = (Monitoria) evento.getComponent().getAttributes().get("monitoriaSelecionada");

            MonitoriaDAO monitoriaDAO = new MonitoriaDAO();
            monitoriaDAO.excluir(monitoria);

            monitorias = monitoriaDAO.listar();

            Messages.addGlobalInfo("Monitoria removida com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Monitoria");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        try {
            monitoria = (Monitoria) evento.getComponent().getAttributes().get("monitoriaSelecionada");
            
            DisciplinaDAO dao = new DisciplinaDAO();
            disciplinas = dao.listar("nome");
            
            Messages.addGlobalInfo("Monitoria editada com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar selecionar uma monitoria");
            erro.printStackTrace();
        }
    }

    @PostConstruct // Executa a função ao abrir a página
    public void listar() {
        try {
            MonitoriaDAO estadoDAO = new MonitoriaDAO();
            monitorias = estadoDAO.listar();
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar as Monitorias");
            erro.printStackTrace();
        }
    }
}
