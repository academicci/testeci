/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.bean;

import br.com.academicci.dao.DisciplinaDAO;
import br.com.academicci.entity.Disciplina;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

@ManagedBean
@ViewScoped
public class DisciplinaBean implements Serializable {

    private Disciplina disciplina;
    private List<Disciplina> disciplinas;

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public void novo() {
        disciplina = new Disciplina();
    }

    public void salvar() {
        try {
            DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
            disciplinaDAO.merge(disciplina);

            novo();
            disciplinas = disciplinaDAO.listar();

            Messages.addGlobalInfo("Disciplina salva com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar a Disciplina!");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            disciplina = (Disciplina) evento.getComponent().getAttributes().get("disciplinaSelecionada");

            DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
            disciplinaDAO.excluir(disciplina);

            disciplinas = disciplinaDAO.listar();

            Messages.addGlobalInfo("Disciplina removida com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Disciplina");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        try {
            disciplina = (Disciplina) evento.getComponent().getAttributes().get("disciplinaSelecionada");

        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar selecionar uma disciplina");
            erro.printStackTrace();
        }
    }

    @PostConstruct // Executa a função ao abrir a página
    public void listar() {
        try {
            DisciplinaDAO estadoDAO = new DisciplinaDAO();
            disciplinas = estadoDAO.listar();
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os estados");
            erro.printStackTrace();
        }
    }
}
