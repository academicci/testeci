/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.bean;

import br.com.academicci.dao.MonitoriaDAO;
import br.com.academicci.dao.AvisoDAO;
import br.com.academicci.dao.PessoaDAO;
import br.com.academicci.entity.Monitoria;
import br.com.academicci.entity.Aviso;
import br.com.academicci.entity.Pessoa;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author alexa
 */
@ManagedBean
@ViewScoped
public class AvisoBean {

    private List<Aviso> avisos;
    private Aviso aviso;
    private List<Monitoria> monitorias;
    private List<Pessoa> pessoas;

    public Aviso getAviso() {
        return aviso;
    }

    public void setAviso(Aviso aviso) {
        this.aviso = aviso;
    }

    public List<Aviso> getAvisos() {
        return avisos;
    }

    public void setAvisos(List<Aviso> avisos) {
        this.avisos = avisos;
    }

    public List<Monitoria> getMonitorias() {
        return monitorias;
    }

    public void setMonitorias(List<Monitoria> monitorias) {
        this.monitorias = monitorias;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public void novo() {
        try {
            aviso = new Aviso();

            MonitoriaDAO mdao = new MonitoriaDAO();
            monitorias = mdao.listar("nome");
            PessoaDAO pessoaDAO = new PessoaDAO();
            pessoas = pessoaDAO.listar("nome");

        } catch (RuntimeException e) {
            Messages.addGlobalError("Erro ao adicionar um novo cliente");
        }

    }

    public void salvar() {
        try {
            AvisoDAO avisoDAO = new AvisoDAO();
            avisoDAO.merge(aviso);

            MonitoriaDAO mdao = new MonitoriaDAO();
            monitorias = mdao.listar("nome");
            PessoaDAO pessoaDAO = new PessoaDAO();
            pessoas = pessoaDAO.listar("nome");

            avisos = avisoDAO.listar("dtAviso");

            aviso = new Aviso();
            Messages.addGlobalInfo("Aviso Salvo com sucesso");

        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar a Aviso!");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            aviso = (Aviso) evento.getComponent().getAttributes().get("avisoSelecionado");

            AvisoDAO avisoDAO = new AvisoDAO();
            avisoDAO.excluir(aviso);

            avisos = avisoDAO.listar();

            Messages.addGlobalInfo("Aviso removida com sucesso!");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Aviso");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        try {
            aviso = (Aviso) evento.getComponent().getAttributes().get("avisoSelecionado");

            MonitoriaDAO dao = new MonitoriaDAO();
            monitorias = dao.listar("nome");

            Messages.addGlobalInfo("Aviso editada com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar selecionar um Aviso");
            erro.printStackTrace();
        }
    }

    @PostConstruct // Executa a função ao abrir a página
    public void listar() {
        try {
            AvisoDAO estadoDAO = new AvisoDAO();
            avisos = estadoDAO.listar();
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os Avisos");
            erro.printStackTrace();
        }
    }

}
