package br.com.academicci.bean;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import br.com.academicci.dao.PessoaDAO;
import br.com.academicci.entity.Pessoa;

@ManagedBean
@SessionScoped
public class AutenticacaoBean {

    private Pessoa usuario;
    private Pessoa usuarioLogado;

    public Pessoa getUsuario() {
        return usuario;
    }

    public void setUsuario(Pessoa usuario) {
        this.usuario = usuario;
    }

    public Pessoa getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Pessoa usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    @PostConstruct
    public void iniciar() {
        usuario = new Pessoa();
    }

    public void autenticar() {
        try {

            PessoaDAO dao = new PessoaDAO();
            usuarioLogado = dao.autenticar(usuario.getMatricula(), usuario.getSenha());

            if (usuarioLogado == null) {
                Messages.addGlobalError("Matrícula ou Senha incorretos!");
                return;
            }

            Faces.redirect("./pages/index.xhtml");
        } catch (IOException e) {
            Messages.addGlobalError("Erro ao redirecionar ===>" + e.getMessage());
            e.printStackTrace();
        }
    }

    public String sair() {
        usuarioLogado = null;
        return "/pages/autenticacaoNew.xhtml?faces-redirect=true";
    }
}
