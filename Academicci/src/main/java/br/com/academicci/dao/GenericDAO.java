package br.com.academicci.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.academicci.util.HibernateUtil;

public class GenericDAO<Entidade> {

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    private Class<Entidade> classe;
    
    private Session sessao;
   

    @SuppressWarnings("unchecked")
    public GenericDAO() { // Ao buscar/listar, busca corretamente
        this.classe = (Class<Entidade>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
        
        this.sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        
    }

    public void salvar(Entidade entidade) {
        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        Transaction transacao = null;

        try {
            transacao = sessao.beginTransaction();
            sessao.save(entidade); //Salva Entidade
            transacao.commit(); //Efetua a transacao de Salvar
        } catch (RuntimeException erro) {
            if (transacao != null) {
                transacao.rollback(); //Reseta tudo se algo deu errado
            }
            throw erro;
        } finally {
            sessao.close(); // Encerra a operacao de fechar a sessao
        }
    }

    @SuppressWarnings("unchecked")
    public Entidade merge(Entidade entidade) {

        Transaction transacao = null;

        try {
            transacao = this.sessao.beginTransaction();
            Entidade retorno = (Entidade) this.sessao.merge(entidade);
            transacao.commit(); //Efetua a transacao de Salvar
            return retorno;
        } catch (RuntimeException erro) {
            if (transacao != null) {
                transacao.rollback(); //Reseta tudo se algo deu errado
            }
            throw erro;
        } finally {
            this.sessao.close(); // Encerra a operacao de fechar a sessao
        }
    }

    public void editar(Entidade entidade) {
        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        Transaction transacao = null;

        try {
            transacao = sessao.beginTransaction();
            sessao.update(entidade);
            transacao.commit();
        } catch (RuntimeException erro) {
            if (transacao != null) {
                transacao.rollback();
            }
            throw erro;
        } finally {
            sessao.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Entidade> listar() {
        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        try {
            Criteria consulta = sessao.createCriteria(classe);
            List<Entidade> resultado = consulta.list();
            return resultado;
        } catch (RuntimeException erro) {
            throw erro;
        } finally {
            sessao.close();
        }
    }

    //listar ordenado
    @SuppressWarnings("unchecked")
    public List<Entidade> listar(String campoOrdenacao) {

        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();

        try {
            Criteria consulta = sessao.createCriteria(classe);
            consulta.addOrder(Order.asc(campoOrdenacao));
            List<Entidade> resultado = consulta.list();
            return resultado;
        } catch (RuntimeException erro) {
            throw erro;
        } finally {
            sessao.close();
        }
    }

    @SuppressWarnings("unchecked")
    public Entidade buscar(Long codigo) {
        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        try {
            Criteria consulta = sessao.createCriteria(classe);
            consulta.add(Restrictions.idEq(codigo));
            Entidade resultado = (Entidade) consulta.uniqueResult();
            return resultado;
        } catch (RuntimeException erro) {
            throw erro;
        } finally {
            sessao.close();
        }
    }

    public void excluir(Entidade entidade) {
        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        Transaction transacao = null;

        try {
            transacao = sessao.beginTransaction();
            sessao.delete(entidade);
            transacao.commit();
        } catch (RuntimeException erro) {
            if (transacao != null) {
                transacao.rollback();
            }
            throw erro;
        } finally {
            sessao.close();
        }
    }

}
