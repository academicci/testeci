package br.com.academicci.dao;

import br.com.academicci.entity.Pessoa;
import br.com.academicci.util.HibernateUtil;
import static javassist.CtMethod.ConstParameter.integer;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class PessoaDAO extends GenericDAO<Pessoa> {

    public Pessoa autenticar(String matricula, String senha) {

        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();

        try {
            Criteria consulta = sessao.createCriteria(Pessoa.class);
//            consulta.createAlias("pessoa", "p");
            consulta.add(Restrictions.eq("matricula", matricula));

            SimpleHash hash = new SimpleHash("md5", senha);
            consulta.add(Restrictions.eq("senha", hash.toHex()));

            Pessoa pessoa = (Pessoa) consulta.uniqueResult();

            return pessoa;

        } catch (RuntimeException e) {
            throw e;
        } finally {
            sessao.close();
        }
    }
    
}
