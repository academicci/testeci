package br.com.academicci.dao;

import br.com.academicci.entity.Monitoria;
import br.com.academicci.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

public class MonitoriaDAO extends GenericDAO<Monitoria> {

    // listar ordenado
    @SuppressWarnings("unchecked")
    public List<Monitoria> listarOrdenado() {

        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();

        try {
            Criteria consulta = sessao.createCriteria(Monitoria.class);
            consulta.createAlias("disciplina", "pn");
            consulta.addOrder(Order.asc("pn.nome"));
            List<Monitoria> resultado = consulta.list();
            return resultado;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            sessao.close();
        }
    }

}
