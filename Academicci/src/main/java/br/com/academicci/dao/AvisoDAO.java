package br.com.academicci.dao;

import br.com.academicci.entity.Aviso;
import br.com.academicci.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

public class AvisoDAO extends GenericDAO<Aviso> {

    // listar ordenado
    @SuppressWarnings("unchecked")
    public List<Aviso> listarOrdenado() {

        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();

        try {
            Criteria consulta = sessao.createCriteria(Aviso.class);
            consulta.createAlias("monitoria", "pn");
            consulta.addOrder(Order.asc("pn.nome"));
            List<Aviso> resultado = consulta.list();
            return resultado;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            sessao.close();
        }
    }

}
