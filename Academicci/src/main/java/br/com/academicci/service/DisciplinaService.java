/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.service;

import br.com.academicci.dao.DisciplinaDAO;
import br.com.academicci.entity.Disciplina;
import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author rafae
 */
@Path("disciplina")
public class DisciplinaService {

    // http://localhost:8080/Academicci/rest/disciplina/
    @GET
    public String listar() {
        DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
        List<Disciplina> disciplinas = disciplinaDAO.listar("nome");

        Gson gson = new Gson();
        String json = gson.toJson(disciplinas);

        return json;
    }

    // http://127.0.0.1:8080/Academicci/rest/disciplina/{codigo}
    // http://127.0.0.1:8080/Academicci/rest/disciplina/3
    @GET
    @Path("{id}")
    public String buscar(@PathParam("id") Long idDisciplina) {
        DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
        Disciplina disciplina = disciplinaDAO.buscar(idDisciplina);

        Gson gson = new Gson();
        String json = gson.toJson(disciplina);

        return json;
    }

    //http://localhost:8080/Academicci/rest/disciplina/
    @PUT
    public String editar(String json) {
        Gson gson = new Gson();
        Disciplina disciplina = gson.fromJson(json, Disciplina.class);

        DisciplinaDAO fabricanteDAO = new DisciplinaDAO();
        fabricanteDAO.editar(disciplina);

        String jsonSaida = gson.toJson(disciplina);
        return jsonSaida;
    }

    //http://localhost:8080/Academicci/rest/disciplina/
    @POST
    public String salvar(String json) {

        Gson gson = new Gson();
        Disciplina disciplina = gson.fromJson(json, Disciplina.class);

        DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
        disciplinaDAO.merge(disciplina);

        String saida = gson.toJson(disciplina);
        return saida;

    }

    //http://localhost:8080/Academicci/rest/disciplina/{id}
    //http://localhost:8080/Academicci/rest/disciplina/3
    @DELETE
    @Path("{id}")
    public String excluir(@PathParam("id") Long idDisciplina) {
        DisciplinaDAO disciplinaDAO = new DisciplinaDAO();

        Disciplina disciplina = disciplinaDAO.buscar(idDisciplina);
        disciplinaDAO.excluir(disciplina);

        Gson gson = new Gson();
        String saida = gson.toJson(disciplina);
        return saida;
    }
}
