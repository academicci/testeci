package br.com.academicci.service;

import br.com.academicci.dao.PessoaDAO;
import br.com.academicci.entity.Pessoa;
import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("pessoa")
public class PessoaService {

    @GET
    @Path("list")
    public String listar() {
        PessoaDAO pessoaDAO = new PessoaDAO();
        List<Pessoa> pessoas = pessoaDAO.listar("nome");

        Gson gson = new Gson();

        String json = gson.toJson(pessoas);
        return json;
    }
    
    @GET
    @Path("{id}")
    public String buscar(@PathParam("id") Long idPessoa) {
        PessoaDAO pessoaDAO = new PessoaDAO();
        Pessoa pessoa = pessoaDAO.buscar(idPessoa);

        Gson gson = new Gson();
        String json = gson.toJson(pessoa);

        return json;
    }

    //http://localhost:8080/Academicci/rest/pessoa/
    @PUT
    public String editar(String json) {
        Gson gson = new Gson();
        Pessoa pessoa = gson.fromJson(json, Pessoa.class);

        PessoaDAO fabricanteDAO = new PessoaDAO();
        fabricanteDAO.editar(pessoa);

        String jsonSaida = gson.toJson(pessoa);
        return jsonSaida;
    }

    //http://localhost:8080/Academicci/rest/pessoa/
    @POST
    public String salvar(String json) {

        Gson gson = new Gson();
        Pessoa pessoa = gson.fromJson(json, Pessoa.class);

        PessoaDAO pessoaDAO = new PessoaDAO();
        pessoaDAO.merge(pessoa);

        String saida = gson.toJson(pessoa);
        return saida;

    }

    //http://localhost:8080/Academicci/rest/pessoa/{id}
    //http://localhost:8080/Academicci/rest/pessoa/3
    @DELETE
    @Path("{id}")
    public String excluir(@PathParam("id") Long idPessoa) {
        PessoaDAO pessoaDAO = new PessoaDAO();

        Pessoa pessoa = pessoaDAO.buscar(idPessoa);
        pessoaDAO.excluir(pessoa);

        Gson gson = new Gson();
        String saida = gson.toJson(pessoa);
        return saida;
    }
}
