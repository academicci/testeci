package br.com.academicci.service;

import br.com.academicci.dao.PerfilDAO;
import br.com.academicci.entity.Perfil;
import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("perfil")
public class PerfilService {

    // http://127.0.0.1:8080/Academicci/rest/perfil
    @GET
//    @Path("/list")
    public String listar() {
        PerfilDAO prefilDAO = new PerfilDAO();
        List<Perfil> perfis = prefilDAO.listar("tipo");

        Gson gson = new Gson();
        String json = gson.toJson(perfis);

        return json;
    }
}
