package br.com.academicci.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

//http://localhost:8080/Academicci/rest/academicci
@Path("academicci")
public class AcademicciService {

    @GET
	public String exibir(){
        return "Gestão de Monitorias";
    }

}
