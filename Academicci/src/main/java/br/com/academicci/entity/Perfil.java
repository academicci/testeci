package br.com.academicci.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

@Entity
public class Perfil extends GenericDomain {

    @Column(nullable = false)
    private Character tipo;

    @ManyToMany(mappedBy = "perfis", fetch = FetchType.EAGER)
    private Set<Pessoa> pessoas = new HashSet<>();

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Set<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(Set<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

}
