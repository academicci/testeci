package br.com.academicci.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class Pessoa extends GenericDomain {

    @Column(length = 7, nullable = false)
    private String matricula;

    @Column(length = 100, nullable = false)
    private String nome;

    @Column(nullable = false)
    private Character sexo;

    @Column(name = "dt_nasc", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dtNasc;

    @Column(length = 100, nullable = false)
    private String email;

    @Column(length = 11, nullable = false)
    private String cpf;

    @Column(length = 25, nullable = false)
    private String rg;

    @Column(columnDefinition = "SMALLINT(2)", nullable = false)
    private Short ddd;
    
    @Column(length = 9, nullable = false)
    private String numero;
    
    @Column(nullable = false)
    private Boolean status;

    @Column(length = 50, nullable = false)
    private String senha;

    @Transient
    private String senhaSemCriptografia;
    

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            joinColumns = {
                @JoinColumn(name = "pessoa_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "perfil_id")})
    private Set<Perfil> perfis = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            joinColumns = {
                @JoinColumn(name = "pessoa_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "monitoria_id")})
    private Set<Monitoria> monitorias = new HashSet<>();

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getDtNasc() {
        return dtNasc;
    }

    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Set<Perfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(Set<Perfil> perfis) {
        this.perfis = perfis;
    }

    public Set<Monitoria> getMonitorias() {
        return monitorias;
    }

    public void setMonitorias(Set<Monitoria> monitorias) {
        this.monitorias = monitorias;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    
    @Transient
    public String getStatusFormatado() {
        String ativoFormatado = "Inativo";
        if (status) {
            ativoFormatado = "Ativo";
        }
        return ativoFormatado;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenhaSemCriptografia() {
        return senhaSemCriptografia;
    }

    public void setSenhaSemCriptografia(String senhaSemCriptografia) {
        this.senhaSemCriptografia = senhaSemCriptografia;
    }

    public Short getDdd() {
        return ddd;
    }

    public void setDdd(Short ddd) {
        this.ddd = ddd;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
     
}
