package br.com.academicci.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Publicacao extends GenericDomain {

    @Column(length = 50, nullable = false)
    private String nome;

    @Column(length = 200, nullable = false)
    private String descricao;

    @Column(length = 30, nullable = true)
    private String tamanho;

    @Column(length = 250, nullable = true)
    private String filePath;

    @Column(name = "dt_publicacao", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dtPublicacao;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Pessoa pessoa;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Monitoria monitoria;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getDtPublicacao() {
        return dtPublicacao;
    }

    public void setDtPublicacao(Date dtPublicacao) {
        this.dtPublicacao = dtPublicacao;
    }

    public Monitoria getMonitoria() {
        return monitoria;
    }

    public void setMonitoria(Monitoria monitoria) {
        this.monitoria = monitoria;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
}
