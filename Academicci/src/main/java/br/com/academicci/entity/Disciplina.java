/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

@Entity
public class Disciplina extends GenericDomain {

    @Column(length = 100, nullable = false)
    private String nome;

    @Column(nullable = false)
    private Boolean status;

    @Column(nullable = false)
    private Short periodo;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Transient
    public String getStatusFormatado() {
        String ativoFormatado = "Inativo";
        if (status) {
            ativoFormatado = "Ativo";
        }
        return ativoFormatado;
    }

    public Short getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Short periodo) {
        this.periodo = periodo;
    }

}
