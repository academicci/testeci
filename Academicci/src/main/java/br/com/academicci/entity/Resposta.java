package br.com.academicci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Resposta extends GenericDomain {

    @Column(nullable = false, columnDefinition = "TEXT(500)")
    private String descricao;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Pergunta pergunta;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Pergunta getPergunta() {
        return pergunta;
    }

    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

}
