
package br.com.academicci.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Monitoria extends GenericDomain{
    
    @Column(nullable = false)
    private String nome;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Disciplina disciplina;
    
    @Column(nullable = false)
    private Boolean status;
    
    @ManyToMany(mappedBy = "monitorias", fetch = FetchType.EAGER)
    private Set<Pessoa> pessoas = new HashSet<>();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    
    @Transient
    public String getStatusFormatado() {
        String ativoFormatado = "Inativo";
        if (status) {
            ativoFormatado = "Ativo";
        }
        return ativoFormatado;
    }

    public Set<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(Set<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

}
