package br.com.academicci.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Aviso extends GenericDomain {

    @Column(length = 100, nullable = false)
    private String assunto;

    @Column(nullable = false, columnDefinition = "TEXT(500)")
    private String descricao;

    @Column(name = "dt_aviso", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dtAviso;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Pessoa pessoa;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Monitoria monitoria;

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtAviso() {
        return dtAviso;
    }

    public void setDtAviso(Date dtAviso) {
        this.dtAviso = dtAviso;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Monitoria getMonitoria() {
        return monitoria;
    }

    public void setMonitoria(Monitoria monitoria) {
        this.monitoria = monitoria;
    }

}
