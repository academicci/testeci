package br.com.academicci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Anexo extends GenericDomain {

    @Column(length = 50, nullable = false)
    private String nome;

    @Column(length = 30, nullable = true)
    private String tamanho;

    @Column(length = 250, nullable = true)
    private String filePath;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Pergunta pergunta;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Resposta resposta;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Pergunta getPergunta() {
        return pergunta;
    }

    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }
    
    
}
