package br.com.academicci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Certificado extends GenericDomain {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Pessoa pessoa;

    @Column(length = 50, nullable = false)
    private String nome;

    @Column(name = "horas_cert", nullable = false)
    private Short horasCert;

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Short getHorasCert() {
        return horasCert;
    }

    public void setHorasCert(Short horasCert) {
        this.horasCert = horasCert;
    }

}
