package br.com.academicci.util;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

//http://localhost:8080/Academicci/rest
@ApplicationPath("rest")
public class AcademicciResourceConfig extends ResourceConfig {
	public AcademicciResourceConfig(){
		packages("br.com.academicci.service");
	}
}
