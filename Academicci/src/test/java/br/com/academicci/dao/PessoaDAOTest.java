package br.com.academicci.dao;

import br.com.academicci.entity.Pessoa;
import br.com.academicci.util.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PessoaDAOTest {

    private Pessoa pessoa = null;
    private Pessoa pessoaErro = null;
    private List<Pessoa> list = null;
    private Class<Pessoa> classe;

    @Mock
    Pessoa pessoaMock = null;
    
    @Mock
    private HibernateUtil hibernateUtil;

    @Before
    public void inicializa() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void preencherPessoa() throws ParseException {
        pessoa = new Pessoa();
        pessoa.setNome("Vinicius Mateus Martins");
        pessoa.setCpf("03041276127");
        pessoa.setDtNasc(new SimpleDateFormat("dd/mm/yyyy").parse("06/05/1996"));
        pessoa.setEmail("teste@teste.com");
        pessoa.setMatricula("1412242");
        pessoa.setRg("326607");
        pessoa.setSexo('M');
        pessoa.setDdd(new Short("62"));
        pessoa.setNumero("992054616");
        pessoa.setStatus(true);
        pessoa.setSenhaSemCriptografia("123456");

        SimpleHash simpleHash = new SimpleHash("md5", pessoa.getSenhaSemCriptografia());

        pessoa.setSenha(simpleHash.toHex());
        
        
    }
    
    @Test
//    @Ignore
    public void salvarPessoaUnitario() throws ParseException {
        PessoaDAO dao = new PessoaDAO();
        Session session = Mockito.mock(Session.class);
        Transaction transaction = Mockito.mock(Transaction.class);
        
        Mockito.when(session.beginTransaction()).thenReturn(transaction);
        
        Mockito.when(session.merge(Matchers.any(Pessoa.class))).thenReturn(this.pessoa);
        
        dao.setSessao(session);
        Pessoa ent = (Pessoa) dao.merge(this.pessoa);
        Assert.assertEquals(pessoa.getCpf(),ent.getCpf());
    }
    
    @Test
    @Ignore
    public void salvarPessoaIntegracao() throws ParseException {
        PessoaDAO dao = new PessoaDAO();

        Pessoa ent = (Pessoa) dao.merge(pessoa);

        Assert.assertEquals(pessoa.getCpf(),ent.getCpf());
    }
    
    @Test
    @Ignore
    public void autenticar() {
        Mockito.when(pessoaMock.getId()).thenReturn(1L);
        Mockito.when(pessoaMock.getMatricula()).thenReturn("1412240");
        Mockito.when(pessoaMock.getSenha()).thenReturn("123456");

        PessoaDAO dao = new PessoaDAO();
        assertNotNull(dao.autenticar(pessoaMock.getMatricula(), pessoaMock.getSenha()));
    }
    
    @Test
    @Ignore
    public void ListarPessoaIntegracao(){
        PessoaDAO dao = new PessoaDAO();
        List<Pessoa> lista = dao.listar();
        
        Assert.assertEquals(lista.size(), dao.listar().size());
    }
    
    @Test
    @Ignore
    public void listarPessoaUnitario(){
        PessoaDAO dao = new PessoaDAO();
        Session session = Mockito.mock(Session.class);
        Transaction transaction = Mockito.mock(Transaction.class);
        
        Criteria consulta = session.createCriteria(classe);
        
        Mockito.when(session.beginTransaction()).thenReturn(transaction);
        
        //this.list = this.pessoa;
        Mockito.when(consulta.list().size()).thenReturn(5);
        
        dao.setSessao(session);
        int lista = consulta.list().size();
        //Assert.assertEquals(lista, 5);
        assertTrue(lista==5);
    }
    
    @Test
    @Ignore
    public void salvarPessoaIntegracaoErro() throws ParseException {
        
        pessoaErro = new Pessoa();
        pessoaErro.setNome("Vinicius Mateus Martins");
        pessoaErro.setCpf("030412767");
        pessoaErro.setDtNasc(new SimpleDateFormat("dd/mm/yyyy").parse("06/05/1996"));
        pessoaErro.setEmail("teste@teste.com");
        pessoaErro.setMatricula("1412242");
        pessoaErro.setRg("326607");
        pessoaErro.setSexo('M');
        pessoaErro.setDdd(new Short("6m"));
        pessoaErro.setNumero("992054616");
        pessoaErro.setStatus(true);
        pessoaErro.setSenhaSemCriptografia("123456");

        SimpleHash simpleHash = new SimpleHash("md5", pessoa.getSenhaSemCriptografia());
        
        pessoa.setSenha(simpleHash.toHex());
        
        PessoaDAO dao = new PessoaDAO();

        Pessoa ent = (Pessoa) dao.merge(pessoaErro);

        Assert.assertEquals(pessoaErro.getCpf(),ent.getCpf());
    }
}
