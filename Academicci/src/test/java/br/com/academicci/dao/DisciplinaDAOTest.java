/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.dao;

import br.com.academicci.entity.Disciplina;
import br.com.academicci.util.HibernateUtil;
import java.text.ParseException;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author alexa
 */
public class DisciplinaDAOTest {
    
    private Disciplina disciplina = null;
    private Disciplina disciplinaErro = null;
    
//    @Mock
//    private HibernateUtil hibernateUtil;

    @Before
    public void inicializa() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void preencherDisciplina() throws ParseException {
        disciplina = new Disciplina();
        
        disciplina.setNome("Teste");
        disciplina.setPeriodo(new Short("7"));
        disciplina.setStatus(true);
        
    }
    
     @Test
    @Ignore
    public void salvarDisciplinaUnitario() throws ParseException {
        DisciplinaDAO dao = new DisciplinaDAO();
        Session session = Mockito.mock(Session.class);
        Transaction transaction = Mockito.mock(Transaction.class);
        
        Mockito.when(session.beginTransaction()).thenReturn(transaction);
        
        Mockito.when(session.merge(Matchers.any(Disciplina.class))).thenReturn(this.disciplina);
        
        dao.setSessao(session);
        Disciplina ent = (Disciplina) dao.merge(this.disciplina);
        Assert.assertEquals(disciplina.getNome(),ent.getNome());
    }
    
    @Test
    @Ignore
    public void salvarDisciplinaTesteIntegracao() throws ParseException {
        DisciplinaDAO dao = new DisciplinaDAO();

        Disciplina ent = (Disciplina) dao.merge(disciplina);

        Assert.assertEquals(disciplina.getNome(),ent.getNome());
    }
    
    @Test
    @Ignore
    public void ListarDisciplinaIntegracao(){
        DisciplinaDAO dao = new DisciplinaDAO();
        List<Disciplina> lista = dao.listar();
        
        Assert.assertEquals(lista.size(), dao.listar().size());
    }
}
