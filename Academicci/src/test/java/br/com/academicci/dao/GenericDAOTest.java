/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.dao;

import br.com.academicci.entity.Pessoa;
import br.com.academicci.util.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author alexa
 */
public class GenericDAOTest {

    private Pessoa pessoa = null;

    @Before
    public void preencherUsuario() throws ParseException {
        pessoa.setNome("TesteInteg");
        pessoa.setCpf("123456789111");
        pessoa.setDtNasc(new SimpleDateFormat("dd/mm/yyyy").parse("06/05/1996"));
        pessoa.setEmail("testeint@testeint.com");
        pessoa.setMatricula("1414144");
        pessoa.setRg("123456");
        pessoa.setSexo('M');
        pessoa.setDdd(new Short("62"));
        pessoa.setNumero("123456789");
        pessoa.setStatus(true);
        pessoa.setSenhaSemCriptografia("123456");

        SimpleHash simpleHash = new SimpleHash("md5", pessoa.getSenhaSemCriptografia());

        pessoa.setSenha(simpleHash.toHex());
    }

    @Test
    @Ignore
    public void conectarBD() {
        Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
        sessao.close();
        HibernateUtil.getFabricaDeSessoes().close();
    }

    @Test
    @Ignore
    public void testSalvar() {
        PessoaDAO dao = new PessoaDAO();
        dao.salvar(pessoa);
    }

    /* @Test
    @Ignore
    public void testMerge() {
        System.out.println("merge");
        Object entidade = null;
        GenericDAO instance = new GenericDAO();
        Object expResult = null;
        Object result = instance.merge(entidade);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    } */

    @Test
    @Ignore
    public void testEditar() {

        PessoaDAO dao = new PessoaDAO();

        pessoa.setId(6L);
        pessoa.setNome("Teste2");

        dao.editar(pessoa);
    }

    @Test
    @Ignore
    public void testListar() {
        PessoaDAO dao = new PessoaDAO();
        List<Pessoa> pessoas = dao.listar();

        assertEquals(2, pessoas.size());
    }


    @Test
    @Ignore
    public void testBuscar() {
        System.out.println("buscar");
        Long codigo = null;
        GenericDAO instance = new GenericDAO();
        Object expResult = null;
        Object result = instance.buscar(codigo);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    @Ignore
    public void testExcluir() {
        PessoaDAO dao = new PessoaDAO();
        dao.excluir(pessoa);
    }

}
