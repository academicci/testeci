//package br.com.academicci.dao;
//
//import br.com.academicci.entity.Pessoa;
//import br.com.academicci.entity.Usuario;
//import java.util.List;
//
//import org.apache.shiro.crypto.hash.SimpleHash;
//import org.junit.Ignore;
//import org.junit.Test;
//
//public class UsuarioDAOTest {
//
//    @Test
//    @Ignore
//    public void salvarUsuario() {
//
//        Long codigoPessoa = 2L;
//
//        PessoaDAO pessoaDAO = new PessoaDAO();
//        Pessoa pessoa = pessoaDAO.buscar(codigoPessoa); // usando a referencia do codigo para os dois tipos
//
//        System.out.println(pessoa.getNome());
//        System.out.println(pessoa.getMatricula());
//
//        Usuario usuario = new Usuario();
//
//        usuario.setPessoa(pessoa);
//        usuario.setStatus(true);
//        usuario.setSenhaSemCriptografia("098765");
//
//        SimpleHash simpleHash = new SimpleHash("md5", usuario.getSenhaSemCriptografia());
//
//        usuario.setSenha(simpleHash.toHex());
//
//        UsuarioDAO dao = new UsuarioDAO();
//        dao.salvar(usuario);
//
//        System.out.println(usuario.getStatus());
//        System.out.println(usuario.getSenha());
//        System.out.println(usuario.getId());
//
//    }
//
//    @SuppressWarnings("unused")
//    @Test
//    @Ignore
//    public void buscar() {
//
//        Long codigo = 1L;
//
//        UsuarioDAO dao = new UsuarioDAO();
//        Usuario ca = new Usuario();
//
//        if (ca == null) {
//            System.out.println("Nenhum usuário encontrado");
//        } else {
//            dao.buscar(codigo);
//            //System.out.println(ca.getCodigo()+" "+ca.getSenha()+" "+ca.getTipo()+" "+ca.getAtivo()+" "+ca.getPessoa().getNome());
//        }
//    }
//
//    @Test
//    @Ignore
//    public void listar() {
//
//        UsuarioDAO dao = new UsuarioDAO();
//
//        List<Usuario> lista = dao.listar();
//
//        System.out.println("Total " + lista.size());
//
//        for (Usuario listas : lista) {
//            System.out.println(listas.getSenha());
//            System.out.println(listas.getStatus());
//            System.out.println(listas.getId());
//            System.out.println(listas.getPessoa().getNome());
//        }
//    }
//
//    @Test
//    @Ignore
//    public void excluir() {
//
//        Long codigo = 3L;
//
//        UsuarioDAO dao = new UsuarioDAO();
//        Usuario us = dao.buscar(codigo);
//
//        if (us == null) {
//            System.out.println("Nenhum usuário encontrado");
//        } else {
//            dao.excluir(us);
//            System.out.println("Usuário excluido com sucesso");
//        }
//    }
//
//    @Test
//    @Ignore
//    public void editar() {
//
//        Long codigo = 1L;
//
//        UsuarioDAO dao = new UsuarioDAO();
//        Usuario us = dao.buscar(codigo);
//
//        if (us == null) {
//            System.out.println("Nenhum resultado encontrado...");
//        } else {
//            us.setSenha("788888888");
//            dao.editar(us);
//
//            System.out.println("Alterações feitas com sucesso ...");
//        }
//    }
//
//    @Test
//    @Ignore
//    public void autenticar() {
//        String cpf = "999.999.999-99";
//        String senha = "q1w2e3r4";
//
//        UsuarioDAO dao = new UsuarioDAO();
//        Usuario usuario = dao.autenticar(cpf, senha);
//
//        System.out.println(usuario);
//    }
//
//}
