/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.academicci.dao;

import br.com.academicci.entity.Disciplina;
import br.com.academicci.entity.Monitoria;
import br.com.academicci.entity.Monitoria;
import br.com.academicci.util.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author alexa
 */
public class MonitoriaDAOTest {
    
    private Disciplina disciplina;
    private Monitoria monitoria = null;
    private Class<Monitoria> classe;

    @Mock
    Monitoria Mock = null;
    
    @Mock
    private HibernateUtil hibernateUtil;

    @Before
    public void inicializa() {
        MockitoAnnotations.initMocks(this);
    }
    
    /*@Before
    public void preencherDisciplina() throws ParseException {
        disciplina = new Disciplina();
        disciplina.setId(new Long("5"));
        disciplina.setNome("Teste");
        disciplina.setPeriodo(new Short("7"));
        disciplina.setStatus(true);
        
    } */

    @Before
    public void preencherMonitoria() throws ParseException {
        disciplina = new Disciplina();
        disciplina.setId(new Long("5"));
        disciplina.setNome("Teste");
        disciplina.setPeriodo(new Short("7"));
        disciplina.setStatus(true);
        
        monitoria = new Monitoria();
        monitoria.setNome("Teste Monitoria");
        monitoria.setDisciplina(disciplina);
        monitoria.setStatus(true);
    }
    
    @Test
    @Ignore
    public void salvarMonitoriaUnitario() throws ParseException {
        MonitoriaDAO dao = new MonitoriaDAO();
        Session session = Mockito.mock(Session.class);
        Transaction transaction = Mockito.mock(Transaction.class);
        
        Mockito.when(session.beginTransaction()).thenReturn(transaction);
        
        Mockito.when(session.merge(Matchers.any(Monitoria.class))).thenReturn(this.monitoria);
        
        dao.setSessao(session);
        Monitoria ent = (Monitoria) dao.merge(this.monitoria);
        Assert.assertEquals(monitoria.getNome(),ent.getNome());
    }
    
    @Test
    @Ignore
    public void salvarMonitoriaIntegracao() throws ParseException {
        MonitoriaDAO dao = new MonitoriaDAO();

        Monitoria ent = (Monitoria) dao.merge(monitoria);

        Assert.assertEquals(monitoria.getNome(),ent.getNome());
    }
    
    @Test
    @Ignore
    public void ListarMonitoriaIntegracao(){
        MonitoriaDAO dao = new MonitoriaDAO();
        List<Monitoria> lista = dao.listar();
        
        Assert.assertEquals(lista.size(), dao.listar().size());
    }
    
}
