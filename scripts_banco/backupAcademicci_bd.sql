CREATE DATABASE  IF NOT EXISTS `academicci_bd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `academicci_bd`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: academicci_bd
-- ------------------------------------------------------
-- Server version	5.7.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anexo`
--

DROP TABLE IF EXISTS `anexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anexo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(250) DEFAULT NULL,
  `nome` varchar(50) NOT NULL,
  `tamanho` varchar(30) DEFAULT NULL,
  `pergunta_id` bigint(20) NOT NULL,
  `resposta_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gswrc5k7uiha56w4lrwrr0mah` (`pergunta_id`),
  KEY `FK_80gyu3sklysu8kp7bfuwsrccu` (`resposta_id`),
  CONSTRAINT `FK_80gyu3sklysu8kp7bfuwsrccu` FOREIGN KEY (`resposta_id`) REFERENCES `resposta` (`id`),
  CONSTRAINT `FK_gswrc5k7uiha56w4lrwrr0mah` FOREIGN KEY (`pergunta_id`) REFERENCES `pergunta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anexo`
--

LOCK TABLES `anexo` WRITE;
/*!40000 ALTER TABLE `anexo` DISABLE KEYS */;
/*!40000 ALTER TABLE `anexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aviso`
--

DROP TABLE IF EXISTS `aviso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aviso` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assunto` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  `dt_aviso` date NOT NULL,
  `monitoria_id` bigint(20) NOT NULL,
  `pessoa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_emhp70rdd1foe50c69kcaiaab` (`monitoria_id`),
  KEY `FK_3uowvs86k6f1n0d8r063k2k1v` (`pessoa_id`),
  CONSTRAINT `FK_3uowvs86k6f1n0d8r063k2k1v` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`),
  CONSTRAINT `FK_emhp70rdd1foe50c69kcaiaab` FOREIGN KEY (`monitoria_id`) REFERENCES `monitoria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aviso`
--

LOCK TABLES `aviso` WRITE;
/*!40000 ALTER TABLE `aviso` DISABLE KEYS */;
/*!40000 ALTER TABLE `aviso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificado`
--

DROP TABLE IF EXISTS `certificado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `horas_cert` smallint(6) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `pessoa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ohfgypjawkjhldvvtagpr0tvh` (`pessoa_id`),
  CONSTRAINT `FK_ohfgypjawkjhldvvtagpr0tvh` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificado`
--

LOCK TABLES `certificado` WRITE;
/*!40000 ALTER TABLE `certificado` DISABLE KEYS */;
/*!40000 ALTER TABLE `certificado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplina` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `periodo` smallint(6) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplina`
--

LOCK TABLES `disciplina` WRITE;
/*!40000 ALTER TABLE `disciplina` DISABLE KEYS */;
INSERT INTO `disciplina` VALUES (2,'TCC II',10,''),(3,'Algoritmos e Programação II',2,'');
/*!40000 ALTER TABLE `disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitoria`
--

DROP TABLE IF EXISTS `monitoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitoria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `status` bit(1) NOT NULL,
  `disciplina_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_abs5wofj86j8gej9q9u3l4wgc` (`disciplina_id`),
  CONSTRAINT `FK_abs5wofj86j8gej9q9u3l4wgc` FOREIGN KEY (`disciplina_id`) REFERENCES `disciplina` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitoria`
--

LOCK TABLES `monitoria` WRITE;
/*!40000 ALTER TABLE `monitoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `monitoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'A'),(2,'A'),(3,'A');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pergunta`
--

DROP TABLE IF EXISTS `pergunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pergunta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(300) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `monitoria_id` bigint(20) NOT NULL,
  `pessoa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_b2ruwgif6rd7fo5nesvsjrwfi` (`monitoria_id`),
  KEY `FK_m82f4d794qbqmejwdbbs77mch` (`pessoa_id`),
  CONSTRAINT `FK_b2ruwgif6rd7fo5nesvsjrwfi` FOREIGN KEY (`monitoria_id`) REFERENCES `monitoria` (`id`),
  CONSTRAINT `FK_m82f4d794qbqmejwdbbs77mch` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pergunta`
--

LOCK TABLES `pergunta` WRITE;
/*!40000 ALTER TABLE `pergunta` DISABLE KEYS */;
/*!40000 ALTER TABLE `pergunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) NOT NULL,
  `ddd` smallint(2) NOT NULL,
  `dt_nasc` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `matricula` varchar(7) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `numero` varchar(9) NOT NULL,
  `rg` varchar(25) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `sexo` char(1) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,'03041276127',62,'1996-01-06','teste@teste.com','1412240','Vinicius Mateus Martins','992054616','326607','e10adc3949ba59abbe56e057f20f883e','M',''),(2,'12345678998',62,'1995-10-03','fulano@detal.com','1412230','João da Silva','990236548','123456','6cf82ee1020caef069e753c67a97a70d','M','');
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa_monitoria`
--

DROP TABLE IF EXISTS `pessoa_monitoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa_monitoria` (
  `pessoa_id` bigint(20) NOT NULL,
  `monitoria_id` bigint(20) NOT NULL,
  PRIMARY KEY (`pessoa_id`,`monitoria_id`),
  KEY `FK_9roaqtsr9ghhhk6m67u4lvsl2` (`monitoria_id`),
  CONSTRAINT `FK_9jtyrfnl6kkm1omplrkb0cl2v` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`),
  CONSTRAINT `FK_9roaqtsr9ghhhk6m67u4lvsl2` FOREIGN KEY (`monitoria_id`) REFERENCES `monitoria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa_monitoria`
--

LOCK TABLES `pessoa_monitoria` WRITE;
/*!40000 ALTER TABLE `pessoa_monitoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoa_monitoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa_perfil`
--

DROP TABLE IF EXISTS `pessoa_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa_perfil` (
  `pessoa_id` bigint(20) NOT NULL,
  `perfil_id` bigint(20) NOT NULL,
  PRIMARY KEY (`pessoa_id`,`perfil_id`),
  KEY `FK_ibccq62iqmeyhebenirgrw42n` (`perfil_id`),
  CONSTRAINT `FK_9na37co8lo8mq6pqfudrimpqh` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`),
  CONSTRAINT `FK_ibccq62iqmeyhebenirgrw42n` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa_perfil`
--

LOCK TABLES `pessoa_perfil` WRITE;
/*!40000 ALTER TABLE `pessoa_perfil` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoa_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacao`
--

DROP TABLE IF EXISTS `publicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(200) NOT NULL,
  `dt_publicacao` date NOT NULL,
  `filePath` varchar(250) DEFAULT NULL,
  `nome` varchar(50) NOT NULL,
  `tamanho` varchar(30) DEFAULT NULL,
  `monitoria_id` bigint(20) NOT NULL,
  `pessoa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_qy7r7f84g33l716011et61evb` (`monitoria_id`),
  KEY `FK_g2ontgc8t7duy4d5bg437exec` (`pessoa_id`),
  CONSTRAINT `FK_g2ontgc8t7duy4d5bg437exec` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`),
  CONSTRAINT `FK_qy7r7f84g33l716011et61evb` FOREIGN KEY (`monitoria_id`) REFERENCES `monitoria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacao`
--

LOCK TABLES `publicacao` WRITE;
/*!40000 ALTER TABLE `publicacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `publicacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resposta`
--

DROP TABLE IF EXISTS `resposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resposta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `pergunta_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kdb9shwfancrcavfx9dqu1ofl` (`pergunta_id`),
  CONSTRAINT `FK_kdb9shwfancrcavfx9dqu1ofl` FOREIGN KEY (`pergunta_id`) REFERENCES `pergunta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resposta`
--

LOCK TABLES `resposta` WRITE;
/*!40000 ALTER TABLE `resposta` DISABLE KEYS */;
/*!40000 ALTER TABLE `resposta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-21 23:35:51
